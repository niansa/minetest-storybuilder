Storybuilder is a game written to create entire stories in Minetest.

Notes:
 * This game is in stable state, but still expect many changes in the future
 * This game is very WIP, so breaking changes could suddenly be made!
 * The mod "minimal" included in this game is a renamed version of "Minetest Minimal Development Test"
 * Some thirdparty mods are included into `mods/thirdparty`
