-- Partitially taken from: https://github.com/TheTermos/wildlife/blob/1c66c50392637d5bb05e4742a707e704c4d79719/init.lua
local function lava_dmg(self,dmg)
	node_lava = node_lava or minetest.registered_nodes[minetest.registered_aliases.mapgen_lava_source]
	if node_lava then
		local pos=self.object:get_pos()
		local box = self.object:get_properties().collisionbox
		local pos1={x=pos.x+box[1],y=pos.y+box[2],z=pos.z+box[3]}
		local pos2={x=pos.x+box[4],y=pos.y+box[5],z=pos.z+box[6]}
		local nodes=mobkit.get_nodes_in_area(pos1,pos2)
		if nodes[node_lava] then mobkit.hurt(self,dmg) end
	end
end

local function predator_brain(self)
--	vitals should be checked every step
	if mobkit.timer(self,1) then lava_dmg(self,6) end
	mobkit.vitals(self)
--	if self.object:get_hp() <=100 then	
	if self.hp <= 0 then
--		cease all activity
		mobkit.clear_queue_high(self)
--		Let the entity go to heaven or hell
		mobkit.hq_die(self)
		return
	end
	
--	decision making needn't happen every engine step
	if mobkit.timer(self,1) then
		local prty = mobkit.get_queue_priority(self)
		
		if prty < 20 and self.isinliquid then
			mobkit.hq_liquid_recovery(self,20)
			return
		end
		
		local pos=self.object:get_pos()
		
--		hunt if not busy with anything important
		if prty < 10 then
--			look for prey
			local prey = mobkit.get_nearby_player(self)
			if prey then 
--				and chase it
				mobkit.hq_hunt(self,10,prey)
			end
		end
		
--		fool around
		if mobkit.is_queue_empty_high(self) then
			mobkit.hq_roam(self,0)
		end
	end
end

minetest.register_entity("entities:test",{
											-- common props
	physical = true,
	stepheight = 0.1,				--EVIL!
	collide_with_objects = true,
	collisionbox = {-0.3, -0.01, -0.3, 0.3, 0.7, 0.3},
	visual = "mesh",
	mesh = "wolf.b3d",
	textures = {"kit_wolf.png"},
	visual_size = {x = 1.3, y = 1.3},
	static_save = true,
	makes_footstep_sound = true,
	on_step = mobkit.stepfunc,	-- required
	on_activate = mobkit.actfunc,		-- required
	get_staticdata = mobkit.statfunc,
											-- api props
	springiness=0,
	buoyancy = 0.75,					-- portion of hitbox submerged
	max_speed = 5,
	jump_height = 1.26,
	view_range = 24,
	lung_capacity = 10, 		-- seconds
	max_hp = 14,
	timeout=600,
	attack={range=0.5,damage_groups={fleshy=7}},
	sounds = {
		attack='dogbite',
		warn = 'angrydog',
		},
	brainfunc = predator_brain,
	
	on_punch=function(self, puncher, time_from_last_punch, tool_capabilities, dir)
		if mobkit.is_alive(self) then
			local hvel = vector.multiply(vector.normalize({x=dir.x,y=0,z=dir.z}),4)
			self.object:set_velocity({x=hvel.x,y=2,z=hvel.z})
			
			mobkit.hurt(self,tool_capabilities.damage_groups.fleshy or 1)

			if type(puncher)=='userdata' and puncher:is_player() then	-- if hit by a player
				mobkit.clear_queue_high(self)							-- abandon whatever they've been doing
				mobkit.hq_hunt(self,10,puncher)							-- get revenge
			end
		end
	end

})
