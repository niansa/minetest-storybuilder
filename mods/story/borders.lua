-- Borders
-- Normal drawtype if buildmode is enabled otherwise airlike
if buildmode == true then
	borderdrawtype = "glasslike"
else
	borderdrawtype = "airlike"
end
-- Register border nodes
minetest.register_node("story:border", {
	description = "Border",
	tiles = {"default_rail_crossing.png"},
	groups = {crumbly=3, soil=1},
	drawtype = borderdrawtype,
	paramtype = "light",
	sunlight_propagates = true,
	walkable = not buildmode,
	pointable = buildmode,
	diggable = buildmode,
	buildable_to = false,
	air_equivalent = true,
})
minetest.register_node("story:border_unwalkable", {
	description = "Border (unwalkable)",
	tiles = {"default_stick.png"},
	groups = {crumbly=3, soil=1},
	drawtype = borderdrawtype,
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	pointable = buildmode,
	diggable = buildmode,
	buildable_to = false,
	air_equivalent = true,
})
