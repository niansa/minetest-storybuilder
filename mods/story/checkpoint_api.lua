local storage = minetest.get_mod_storage()

-- init
function story.init(name)
--	Initalize database
	local data = {checkpoint = false, checkpoint_x = 0, checkpoint_y = 0, checkpoint_z = 0}
	storage:set_string(name, minetest.serialize(data))
	return true
end

-- get
function story.get_checkpoint(name)
	local data = minetest.deserialize(storage:get_string(name))
	if not data or not data.checkpoint then
		return false
	end
	local checkpoint = {x = data.checkpoint_x, y = data.checkpoint_y, z = data.checkpoint_z}
	return checkpoint
end

-- set
function story.set_checkpoint(name, enabled, pos_x, pos_y, pos_z)
	local data = minetest.deserialize(storage:get_string(name))
	if not enabled then
		data.checkpoint = false
	else
		data.checkpoint_x = pos_x
		data.checkpoint_y = pos_y
		data.checkpoint_z = pos_z
		data.checkpoint = true
	end
	storage:set_string(name, minetest.serialize(data))
	return true
end
