schem = {}

-- runcmd
function runcmd(owner, cmd, param)
	local command = minetest.chatcommands[cmd]
	return command.func(owner, param)
end
-- loadschem
function schem.load(name, spos, schem)
	runcmd(name, "/fixedpos", "set1 "..spos.x.." "..spos.y.." "..spos.z)
	runcmd(name, "/load", schem)
	runcmd(name, "/reset", "")
end
