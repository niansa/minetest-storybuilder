minetest.register_globalstep(function(dtime)
	local objs = minetest.object_refs
	for id, obj in pairs(objs) do
		if obj:is_player() then
--			Get player and environment informations
			local pos = obj:get_pos()
			local name = obj:get_player_name()
			local node = minetest.get_node(pos)

--			Check if the node that the player is inside is a stageblock or checkpoint
			if string.match(node.name, "storybase:stageblock_") then
--				Set stage to new one
				storybase.set_stage(name, node.name:gsub("storybase:stageblock_", ""), false)
--				try to play warp sound
				pcall(function () minetest.sound_play("warp", {to_player=name, gain=2.00}) end)
--				Finally, reload the stage
				story.reload_stage(name)
            
			elseif node.name == "story:checkpoint" then
				local rel_pos = story.get_player_relative_pos(name)
				story.set_checkpoint(name, true, rel_pos["x"], rel_pos["y"], rel_pos["z"])
			end

		end
	end
end)
