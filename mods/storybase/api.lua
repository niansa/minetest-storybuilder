local storage = minetest.get_mod_storage()
local worldpath = minetest.get_worldpath()
storybase = {}

-- init
function storybase.init(name)
--	Initalize database
	local data = { stage = 1, score = 0, extrastage = nil}
	storage:set_string(name, minetest.serialize(data))
--	Initalize privileges
	minetest.set_player_privs(name, defaultprivs)
	return true
end

-- check
function storybase.check(name)
--	Read data from database
	local data = minetest.deserialize(storage:get_string(name))
--	Check if all required keys do exist
	if not data.stage then
		data.stage = 1
		did_reset = true
	end
	if not data.score then
		data.score = 0
		did_reset = true
	end
    if did_reset then
		minetest.chat_send_player(name, "Some of your game data required a reset. We're sorry for this bug, but already working on it.")
	end
    
	data.stage = data.stage
	data.score = tonumber(data.score)
	storage:set_string(name, minetest.serialize(data))
	return true
end

-- list
function storybase.list_stages()
	local stagelist = {}
	local rawlist = minetest.get_dir_list(worldpath.."/stages/", false)
	for index, rawentry in pairs(rawlist) do
		table.insert(stagelist, string.reverse(string.gsub(rawentry:reverse(),"sop.", "", 1)))
	end
	return stagelist
end

-- get
function storybase.get_score(name)
	local data = minetest.deserialize(storage:get_string(name))
	return data.score
end
function storybase.get_stage(name)
	local data = minetest.deserialize(storage:get_string(name))
	return data.stage
end

-- set
function storybase.set_score(name, newscore)
	local data = minetest.deserialize(storage:get_string(name))
	data.score = newscore
	storage:set_string(name, minetest.serialize(data))
	return true
end
function storybase.set_stage(name, newstage)
--	Check for existence
	if newstage and not story.get_stage(newstage) then
		storybase.crashmsg(name, "set_stage", "No such stage", newstage)
		return false
    end
--	Disable checkpoint
	story.set_checkpoint(name, false)
--	Store data
	local data = minetest.deserialize(storage:get_string(name))
	data.stage = newstage
	storage:set_string(name, minetest.serialize(data))
    story.clean_stagevars()
	return true
end

-- inc
function storybase.inc_score(name, amount)
	oldscore = storybase.get_score(name)
	newscore = oldscore + amount
	storybase.set_score(name, newscore)
	return newscore
end

-- debugmsg
function storybase.debugmsg(name, message)
	if debugmsgs == true then
		minetest.chat_send_player(name, "Debug: "..message)
		return false
	end
	minetest.log("info", "("..name..") "..message)
	return true
end

-- crashmsg
function storybase.crashmsg(name, where, what, which)
	return minetest.kick_player(name, "\nAn internal error occured in "..where..": "..what..": "..which.."\nPlease report this to the developer of this story!")
end
