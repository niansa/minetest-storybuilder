local modpath = minetest.get_modpath("storybase")
local worldpath = minetest.get_worldpath()
dofile(modpath.."/config.lua")
dofile(modpath.."/api.lua")
dofile(modpath.."/commands.lua")
dofile(modpath.."/nodes.lua")
dofile(modpath.."/globalsteps.lua")

-- Initalize server
minetest.after(0, function()
--	Nothing yet
end)


-- Initalize new players
minetest.register_on_newplayer(function(player)
	local name = player:get_player_name()
--	Give initial stuff
	player:get_inventory():add_item('main', 'minimal:pick_stone')
	player:get_inventory():add_item('main', 'minimal:axe_stone')
	player:get_inventory():add_item('main', 'minimal:shovel_stone')
--	Create database entries
	storybase.init(name)
	story.init(name)
end)

-- Initalize joining players
minetest.register_on_joinplayer(function(player)
--	Check players data
	storybase.check(player:get_player_name())
--	Load current stage
	player:hud_set_flags(defaulthud)
	story.reload_stage(player:get_player_name(), true)
end)
