buildmode = false
noreload = false
allow_cheating = minetest.is_singleplayer()
debugmsgs = true
defaultprivs = {
	interact = true,
	shout = true,
	fast = true
}
defaulthud = {
	hotbar = true,
	crosshair = true,
	wielditem = true,
	breathbar = true,
	minimap = true,
	minimap_radar = true,
	healthbar = true
}
defaultnohud = {
	hotbar = false,
	crosshair = false,
	wielditem = false,
	breathbar = false,
	minimap = false,
	minimap_radar = false,
	healthbar = false
}
