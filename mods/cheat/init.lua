-- Init some data
local pick_admin_toolcaps = {
	full_punch_interval = 0.1,
	max_drop_level = 3,
	groupcaps = {
		unbreakable = {times = {[1] = 0, [2] = 0, [3] = 0}, uses = 0, maxlevel = 3},
		fleshy = {times = {[1] = 0, [2] = 0, [3] = 0}, uses = 0, maxlevel = 3},
		choppy = {times = {[1] = 0, [2] = 0, [3] = 0}, uses = 0, maxlevel = 3},
		bendy = {times = {[1] = 0, [2] = 0, [3] = 0}, uses = 0, maxlevel = 3},
		cracky = {times = {[1] = 0, [2] = 0, [3] = 0}, uses = 0, maxlevel = 3},
		crumbly = {times = {[1] = 0, [2] = 0, [3] = 0}, uses = 0, maxlevel = 3},
		snappy = {times = {[1] = 0, [2] = 0, [3] = 0}, uses = 0, maxlevel = 3},
	},
	damage_groups = {fleshy = 1000},
}
cheatinginfo = "Cheating allows you to:\n1. Use advanced but experimental commands (see /cheating commands)\n2. Fly (default key K)\n3. Noclip \"Ghost mode\" (default key H)"
cheatingcmds = "Commands: /getscore, /getstage, /setscore, /setstage, /incscore, /incstage, /loadstage, /reloadstage\nWARNING: Bad syntax is fatal"
cheatingparams = "<allow/disallow/info/commands>"

-- Cheating privilege
minetest.register_privilege("cheat", {
    description = "is allowed to cheat",
    give_to_singleplayer = false
})

-- Tools
minetest.register_tool("cheat:pick", {
	description = "Pickaxe",
	range = 20,
	inventory_image = "default_tool_mesepick.png",
	tool_capabilities = pick_admin_toolcaps,
})

-- API
cheat = {}
function cheat.dirtyclearchat(name)
	local i = 1
	while i ~= 15 do
		minetest.chat_send_player(name, " ")
		i = i + 1
	end
end

-- Commands
if allow_cheating == true then
minetest.register_chatcommand("cheating", {
	params = cheatingparams,
	func = function(name, param)
		if param == "allow" then
--			Get current privileges
			local privs = minetest.get_player_privs(name)
--			Grant "cheat" privilege
			privs.cheat = true
			privs.fly = true
			privs.noclip = true
			minetest.set_player_privs(name, privs)
			print("[cheat] "..name.." is now allowed to cheat.")
			return true, "Cheating allowed"
		elseif param == "disallow" then
--			Get current privileges
			local privs = minetest.get_player_privs(name)
--			Revoke "cheat" privilege
			privs.cheat = nil
			privs.fly = nil
			privs.noclip = nil
			minetest.set_player_privs(name, privs)
			print("[cheat] "..name.." is no longer allowed to cheat.")
			return true, "Cheating disallowed"
		elseif param == "info" then
			return true, cheatinginfo
		elseif param == "commands" then
			return true, cheatingcmds
		else
			return true, "Syntax: /cheating "..cheatingparams
		end
	end
})
end
minetest.register_chatcommand("getscore", {
	privs = "cheat",
	func = function(name, param)
		local score = storybase.get_score(name)
		return true, "Your current score: " .. score
	end
})
minetest.register_chatcommand("getstage", {
	privs = "cheat",
	func = function(name, param)
		local stage = storybase.get_stage(name)
		return true, "Your current stage: " .. stage
	end
})
minetest.register_chatcommand("setscore", {
	privs = "cheat",
	func = function(name, param)
		storybase.set_score(name, tonumber(param))
		return true, "Your new score: " .. param
	end
})
minetest.register_chatcommand("setstage", {
	privs = "cheat",
	func = function(name, param)
		storybase.set_stage(name, param)
		return true, "Your new stage: " .. param
	end
})
minetest.register_chatcommand("incscore", {
	privs = "cheat",
	func = function(name, param)
		local score = storybase.inc_score(name, tonumber(param))
		return true, "Score was increased to: " .. score
	end
})
minetest.register_chatcommand("incstage", {
	privs = "cheat",
	func = function(name, param)
		local stage = storybase.inc_stage(name, tonumber(param))
		return true, "Stage was increased to: " .. stage
	end
})
minetest.register_chatcommand("reloadstage", {
	privs = "cheat",
	func = function(name, param)
		if param == "continue" then
			continuation = true
		else
			continuation = false
		end
		return story.reload_stage(name, continuation)
	end
})
minetest.register_chatcommand("loadstage", {
	privs = "cheat",
	func = function(name, param)
		storybase.set_stage(name, tonumber(param))
		return story.reload_stage(name)
	end
})
