-- This is just an API
bgmusic = {}
local pmusic = {}
pmusic.id = {}
pmusic.title = {}

function bgmusic.play(name, sound)
	if sound ~= nil then
		pmusic.id[name] = minetest.sound_play(sound, {to_player=name, gain=0.75, loop=true})
		pmusic.title[name] = sound
		return true
	end
	return false
end
function bgmusic.stop(name)
	if pmusic.id[name] then
		minetest.sound_stop(pmusic.id[name])
        pmusic.title[name] = nil
        pmusic.id[name] = nil
		return true
	end
	return false
end
function bgmusic.playing(type, name)
	if type == "id" then
		return pmusic.id[name]
	elseif type == "title" then
		return pmusic.title[name]
	else
		return false
	end
end

minetest.register_on_leaveplayer(function(player)
	pmusic.id[player:get_player_name()] = nil
	pmusic.name[player:get_player_name()] = nil
end)
