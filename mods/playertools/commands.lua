-- gamereset
minetest.register_chatcommand("gamereset", {
	privs = "interact",
	func = function(name, param)
--		Check if current game is a singleplayer game
		if minetest.is_singleplayer() then
			return false, "You are in singleplayer mode, so your game data can't be removed automatically."
		end
--		Make sure the player does not run this by accident
		if param ~= "I am sure" then
			return false, "Use following command to agree that all your data will be removed: \"/gamereset I am sure\""
		else
			minetest.kick_player(name, "Your gamedata is being removed, so you have to reconnect now.")
			playertools.remove(name)
			return true
		end
	end
})
