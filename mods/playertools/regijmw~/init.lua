local modpath = minetest.get_modpath("corruptionhandler")
dofile(modpath.."/api.lua")
dofile(modpath.."/commands.lua")

-- Corruption privilege
minetest.register_privilege("corruption", {
    description = "Is corrupted",
    give_to_singleplayer = false
})
